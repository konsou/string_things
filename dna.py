import time


class Thingy:
    def __init__(self, start_pos, width, movement):
        self.start_pos = start_pos
        self.width = width
        self.movement = movement

    def _get_end_pos(self):
        return self.start_pos + self.width - 1

    def is_in_position(self, position):
        if self.start_pos <= position <= self.end_pos:
            return True
        return False

    def update(self):
        self.start_pos += self.movement

    end_pos = property(_get_end_pos)


if __name__ == '__main__':
    con_width = 80
    things = [Thingy(2, 3, 1), Thingy(76, 3, -1), Thingy(30, 2, 1.5), Thingy(50, 2, -1.5)]

    while True:
        for t in things:
            t.update()
            if t.start_pos < 0:
                t.movement *= -1
                t.start_pos = 0
            elif t.end_pos > con_width:
                t.movement *= -1
                t.start_pos = con_width - t.width

        stringi = ""
        for i in range(con_width):
            chaari = " "
            for t in things:
                if t.is_in_position(i):
                    chaari = "#"
                    break
            stringi += chaari
        print(stringi)
        time.sleep(0.01)
