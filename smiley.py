from time import sleep
from typing import Tuple
from pygame.math import Vector2

CONSOLE_WIDTH = 80
CONSOLE_HEIGHT = 24


class MutableString:
    def __init__(self, value: str) -> None:
        # str.__init__(self)
        self._value = list(value)
        # print(self._value)

    def __setitem__(self, key, value):
        # Extend length if set over the index at the end
        try:
            self._value[key] = value
        except IndexError as e:
            # print(f"key {key}, len {len(self._value)}")
            if key > 0:
                for _ in range(key - len(self._value) + 1):
                    self._value.append(" ")
                # print(f"key {key}, len {len(self._value)}")
                self._value[key] = value
            else:
                raise

    def __getitem__(self, item):
        return self._value[item]

    def __repr__(self):
        return f"{self.__class__.__name__}: \"{''.join(self._value)}\""

    def __str__(self):
        return "".join(self._value)

    def __len__(self):
        return len(self._value)


class LineColumnString:
    def __init__(self, value: str) -> None:
        self._lines = [MutableString(line_) for line_ in value.split("\n")]
        # print(self._lines)

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}>:\n\"\"\"{str(self)}\"\"\""

    def __str__(self) -> str:
        ret_val = ""
        for line_ in self._lines:
            ret_val += f"{str(line_)}\n"
        return ret_val[:-1]  # remove last newline

    def __getitem__(self, item):
        # print(self)
        # print(self._lines[item])
        # print(f"{self.__class__.__name__} __getitem__: item: {item}")
        return self._lines[item]

    def __setitem__(self, key: int, value: str):
        self._lines[key] = MutableString(value)

    def __iter__(self):
        yield from self._lines

    def _get_number_of_lines(self) -> int:
        return len(self._lines)

    def _get_number_of_columns(self) -> int:
        widest = 0
        for line_ in self._lines:
            if len(line_) > widest:
                widest = len(line_)
        return widest

    def _get_center_coordinates(self) -> Tuple[int, int]:
        """ Return the center coordinates as (line, column) """
        return self.number_of_lines // 2, self.number_of_columns // 2

    def rotate(self, degrees: float, center_offset_x: int = 0, center_offset_y: int = 0) -> 'LineColumnString':
        """ Returns a copy of self, rotated clockwise n degrees """
        one_line = f"{' ' * self.number_of_columns}\n"
        ret_copy = LineColumnString(one_line * self.number_of_lines)
        # print(repr(ret_copy))
        # Pygame Vector2 rotates counter-clockwise - invert degrees to get clockwise rotation
        degrees = -degrees
        center_y, center_x = self.center_coordinates
        center_x += center_offset_x
        center_y += center_offset_y
        # VECTOR USES (x, y)! NOT (y, x)
        center_point = Vector2(center_x, center_y)

        for line_number, line in enumerate(self._lines):
            for column_number, character in enumerate(line):
                if not character == " ":
                    # print(f"line {line_number}, character {column_number}: {character}")
                    current_point = Vector2(column_number, line_number)
                    point_offset = current_point - center_point
                    # print(f"Offset from center ({center_point}): {point_offset}")
                    rotated_offset = point_offset.rotate(degrees)
                    # print(f"Offset after rotation: {rotated_offset}")
                    new_point = rotated_offset + center_point
                    new_column, new_line = round(new_point[0]), round(new_point[1])
                    # print(f"new col: {new_column}, new line: {new_line}")
                    try:
                        ret_copy[new_line][new_column] = character
                    except IndexError:
                        pass

        return ret_copy

    number_of_lines = property(_get_number_of_lines)
    number_of_columns = property(_get_number_of_columns)
    center_coordinates = property(_get_center_coordinates)


SMILEY_STRING = LineColumnString(
      f"{'##      ##'.center(CONSOLE_WIDTH)}\n"
      f"{'##      ##'.center(CONSOLE_WIDTH)}\n"
      f"{'##      ##'.center(CONSOLE_WIDTH)}\n"
      f"{'##      ##'.center(CONSOLE_WIDTH)}\n"
      f"{'##      ##'.center(CONSOLE_WIDTH)}\n"
 f"{'##                ##'.center(CONSOLE_WIDTH)}\n"
  f"{'##              ##'.center(CONSOLE_WIDTH)}\n"
   f"{'###          ###'.center(CONSOLE_WIDTH)}\n"
    f"{'##############'.center(CONSOLE_WIDTH)}\n"
     f"{'############'.center(CONSOLE_WIDTH)}\n"
)

# columns = SMILEY_STRING.number_of_columns
# more_columns = " " * (columns // 2)

lines = SMILEY_STRING.number_of_lines
# print(f"Original smiley lines: {lines}")
one_empty_line = "".center(CONSOLE_WIDTH - 1) + "\n"
# print(f"One empty line length: {len(one_empty_line)}")
# input()
empty_lines = one_empty_line * ((CONSOLE_HEIGHT - lines) // 2)
# print(f"Empty lines START:\n{empty_lines}:END")
empty_lines_len = len(empty_lines.split("\n"))
# print(f"{empty_lines_len} lines")
# input()

# print(f"EMPTY LINES: \"{empty_lines}\"")
SMILEY_STRING = LineColumnString(f"{empty_lines}{SMILEY_STRING}{empty_lines}")


# print(SMILEY_STRING)

center_line, center_column = SMILEY_STRING.center_coordinates
print(center_line, center_column)
# SMILEY_STRING[center_line][center_column] = "O"
print(SMILEY_STRING)
# print(SMILEY_STRING.number_of_columns, SMILEY_STRING.number_of_lines)
input("press enter")
# print(SMILEY_STRING.rotate(90))

offset_radius = 0
offset_angle = 0
offset_radius_change = 0.1
offset_angle_change = 4
MAX_OFFSET_RADIUS = 30
MIN_OFFSET_RADIUS = 0
CHANGLE_ANGLE_DIRECTION_EVERY_X_FRAMES = 50
frame_counter = 0
SLEEP_TIME = 0.03

while True:
    for angle in range(0, 360, 5):
        frame_counter += 1
        offset_radius += offset_radius_change
        if offset_radius > MAX_OFFSET_RADIUS or offset_radius < MIN_OFFSET_RADIUS:
            offset_radius_change = -offset_radius_change

        if frame_counter % CHANGLE_ANGLE_DIRECTION_EVERY_X_FRAMES == 0:
            offset_angle_change = -offset_angle_change

        offset_angle += offset_angle_change

        while offset_angle >= 360:
            offset_angle -= 360
        while offset_angle < 0:
            offset_angle += 360

        offset_vector = Vector2(offset_radius, 0)

        print(SMILEY_STRING.rotate(angle, *offset_vector.rotate(offset_angle)))
        sleep(SLEEP_TIME)
